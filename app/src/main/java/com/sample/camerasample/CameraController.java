package com.sample.camerasample;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import com.omkingo.xlib.CommonTools;

import pl.aprilapps.easyphotopicker.EasyImage;

public class CameraController {
    public final static int PERMISSION_TAKE_PHOTO = 23;
    public final static int PERMISISON_PICK_GALLERY = 24;

    public final static int TAKE_PHOTO = 123;
    public final static int PICK_GALLERY = 124;

    public static void takePhoto(Activity activity) {
        if  (CommonTools.isGrantedAllPermission(activity, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }))
            EasyImage.openCamera(activity, TAKE_PHOTO);
        else
            CommonTools.appActivityRequestPermission(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_TAKE_PHOTO);
    }

    public static void pickGallery(Activity activity) {
        if  (CommonTools.isGrantedAllPermission(activity, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }))
            EasyImage.openGallery(activity, PICK_GALLERY);
        else
            CommonTools.appActivityRequestPermission(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISISON_PICK_GALLERY);
    }



    public static boolean checkGalleryAppAvailability(Context context) {
        return EasyImage.canDeviceHandleGallery(context);
    }


}
