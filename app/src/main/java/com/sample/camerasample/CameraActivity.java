package com.sample.camerasample;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sample.camerasample.service.ApiBuilder;
import com.sample.camerasample.service.ProgressRequestBody;
import com.sample.camerasample.service.ServiceController;

import java.io.File;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {

    FloatingActionButton gallery;
    FloatingActionButton photo;

    ImageView imageView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindView();
        setupView();
        setupInstance();
        activityReady();
    }

    private void bindView(){
        imageView = findViewById(R.id.imageView);
        gallery = findViewById(R.id.gallery);
        photo = findViewById(R.id.photo);
        progressBar = findViewById(R.id.progressbar);
    }

    private void setupView() {
        gallery.setOnClickListener(this);
        photo.setOnClickListener(this);

        if  (!CameraController.checkGalleryAppAvailability(this)){
            gallery.setVisibility(View.GONE);
        }
    }

    private void setupInstance(){
    }

    private void activityReady(){
        EasyImage.configuration(this)
                .setImagesFolderName("camera sample")
                .setAllowMultiplePickInGallery(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CameraController.PERMISSION_TAKE_PHOTO:
                CameraController.takePhoto(this);
                break;
            case CameraController.PERMISISON_PICK_GALLERY:
                CameraController.pickGallery(this);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.photo:
                CameraController.takePhoto(this);
                break;
            case R.id.gallery:
                CameraController.pickGallery(this);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(CameraActivity.this, "Uploading", Toast.LENGTH_SHORT).show();

                Glide.with(CameraActivity.this).load(imageFiles.get(0)).into(imageView);

                new ServiceController().upload(imageFiles.get(0),CameraActivity.this, new ServiceController.ApiCallback() {
                    @Override
                    public void result() {
                        Toast.makeText(CameraActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void failed(String str) {
                        Toast.makeText(CameraActivity.this, "Failed: " + str, Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(CameraActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {
        progressBar.setProgress(percentage);
    }

    @Override
    public void onError() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFinish() {
        progressBar.setProgress(100);
    }
}
