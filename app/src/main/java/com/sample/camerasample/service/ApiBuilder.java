package com.sample.camerasample.service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBuilder {

    private static final String BASE_DEV_URL = "https://scb-jenbodin.firebaseapp.com/";

    private Retrofit retrofit;

    public ApiBuilder(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_DEV_URL)
                .client(getLogClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private OkHttpClient getLogClient(){
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        builder.addInterceptor(interceptor);

        return builder.build();
    }

    public ApiEndpoint create() {
        return retrofit.create(ApiEndpoint.class);
    }
}
