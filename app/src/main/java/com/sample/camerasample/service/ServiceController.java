package com.sample.camerasample.service;

import com.sample.camerasample.CameraActivity;
import com.sample.camerasample.service.model.Response;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ServiceController {

    private ApiBuilder apiBuilder;

    public ServiceController(){
        this.apiBuilder = new ApiBuilder();
    }

    public void upload(File file, ProgressRequestBody.UploadCallbacks progress,  final ApiCallback callback){
        ProgressRequestBody fileBody = new ProgressRequestBody(file, progress);

        MultipartBody.Part image = MultipartBody.Part.createFormData("Upload Image", file.getName(), fileBody);

        final Call<Response> call = this.apiBuilder.create().upload(image);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                callback.result();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                callback.failed(t.getMessage());
            }
        });
    }

    public interface ApiCallback {
        void result();
        void failed(String str);
    }
}