package com.sample.camerasample.service;

import com.sample.camerasample.service.model.Response;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiEndpoint {

    @Multipart
    @POST("upload.json")
    Call<Response> upload(@Part MultipartBody.Part image);

}